import java.util.Scanner;
import java.util.Random;

package RandomNumGuess;

public class GuessRandom {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int size = 10;
        int originArr[] = new int[size];
        System.out.println("Let begin Game");
        System.out.println("What is your Name?");
        String name = in.nextLine();
        System.out.println("Try to guess Number");
        int random = (int) (Math.random() * 100);
        int count = -1;
        int tempArray[] = null;
        while (true) {
            System.out.println("Write your number");
            int num = in.nextInt();
            count++;
            if (count > size - 1) {
                size += 10;
                System.out.println("size:" + size);
                tempArray = new int[size];
                for (int i = 0; i < originArr.length; i++) {
                    tempArray[i] = originArr[i];
                }
                originArr = new int[size];
                for (int i = 0; i < originArr.length; i++) {
                    originArr[i] = tempArray[i];
                }
            }
            originArr[count] = num;
            System.out.println();
            if (num == random) {
                System.out.print("Your numbers: ");
                for (int i = 0; i < originArr.length; i++) {
                    if (originArr[i] != 0)
                        System.out.print(originArr[i]+" ");
                      }
                System.out.println();
                System.out.println("Congratulations " + name + "!");
                break;
            } else if (num < random) {
                System.out.println("Your number is too small. Please, Try again... ");
            } else {
                System.out.println("Your number is too large. Please, Try again... ");
            }
        }
    }
}
